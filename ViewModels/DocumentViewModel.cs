using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using KVV.DocumentManagement.Models;

namespace KVV.DocumentManagement.ViewModels {
    public class DocumentViewModel {
        public IList<DocumentGroupPart> Groups { get; set; }
        public DocumentPart CurrentDocument { get; set; }

        [Required]
        public int? GroupId { get; set; }

        public bool Delete { get; set; }
    }
}