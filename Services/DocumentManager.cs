﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using KVV.DocumentManagement.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Core.Title.Models;
using Orchard.Environment.Extensions;

namespace KVV.DocumentManagement.Services {
    public interface IDocumentManager : IDependency {
        IEnumerable<DocumentPart> Fetch(VersionOptions options, string searchTerm = null);
        IEnumerable<DocumentPart> Fetch(VersionOptions options, int skip, int count, string searchTerm = null);
        int Count(VersionOptions options, string searchTerm = null);
        DocumentPart Get(int id, VersionOptions options);
        void Delete(DocumentPart document);
        void StoreFile(DocumentPart part, HttpPostedFileBase file);
        void DeleteFile(DocumentPart document);
        Stream LoadFile(DocumentPart document);
        IEnumerable<DocumentPart> GetByGroup(DocumentGroupPart group, VersionOptions options);
        void Publish(DocumentPart document);
        void Unpublish(DocumentPart document);
        void AssignGroup(List<DocumentPart> documents, DocumentGroupPart group);
    }

    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentManager : IDocumentManager {
        private readonly IContentManager _contentManager;
        private readonly IDocumentStorageProvider _documentStorage;

        public DocumentManager(IContentManager contentManager, IDocumentStorageProvider documentStorage) {
            _documentStorage = documentStorage;
            _contentManager = contentManager;
        }

        public IEnumerable<DocumentPart> Fetch(VersionOptions versionOptions, string searchTerm = null) {
            return GetDocumentsQuery(versionOptions, searchTerm);
        }

        public IEnumerable<DocumentPart> Fetch(VersionOptions versionOptions, int skip, int count, string searchTerm = null) {
            return GetDocumentsQuery(versionOptions, searchTerm).Skip(skip).Take(count);
        }

        public int Count(VersionOptions versionOptions, string searchTerm = null) {
            return GetDocumentsQuery(versionOptions, searchTerm).Count();
        }

        public DocumentPart Get(int id, VersionOptions versionOptions) {
            return _contentManager.Get<DocumentPart>(id, versionOptions);
        }

        public void Delete(DocumentPart document) {
            _contentManager.Remove(document.ContentItem);
        }

        public void StoreFile(DocumentPart document, HttpPostedFileBase file) {
            _documentStorage.Delete(document);
            document.OriginalFileName = Path.GetFileName(file.FileName);
            _documentStorage.Store(document, file.InputStream);
        }

        public void DeleteFile(DocumentPart document) {
            _documentStorage.Delete(document);
        }

        public Stream LoadFile(DocumentPart document) {
            return _documentStorage.Load(document);
        }

        public IEnumerable<DocumentPart> GetByGroup(DocumentGroupPart group, VersionOptions versionOptions) {
            return _contentManager.Query<DocumentPart, DocumentPartRecord>().ForVersion(versionOptions).Where(x => x.GroupId == group.Id).List();
        }

        public void Publish(DocumentPart document) {
            _contentManager.Publish(document.ContentItem);
        }

        public void Unpublish(DocumentPart document) {
            _contentManager.Unpublish(document.ContentItem);
        }

        public void AssignGroup(List<DocumentPart> documents, DocumentGroupPart group) {
            foreach (var document in documents) {
                document.Group = group;
            }
        }

        private IEnumerable<DocumentPart> GetDocumentsQuery(VersionOptions versionOptions, string searchTerm = null) {
            var query = _contentManager.HqlQuery<DocumentPart>()
                .OrderBy(alias => alias.ContentPartRecord<TitlePartRecord>(), x => x.Asc("Title"))
                .ForVersion(versionOptions);
            
            if (!string.IsNullOrWhiteSpace(searchTerm)) {
                query.Where(
                    alias => alias.ContentPartRecord<TitlePartRecord>(),
                    factory => factory.InsensitiveLike("Title", searchTerm, HqlMatchMode.Anywhere));

            }

            return query.List();
        }
    }

}