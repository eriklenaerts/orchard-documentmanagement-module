﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Records;
using Orchard.ContentManagement.Utilities;
using Orchard.Environment.Extensions;

namespace KVV.DocumentManagement.Models {
    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentListPart : ContentPart<DocumentListPartRecord> {
        internal LazyField<DocumentGroupPart> GroupField = new LazyField<DocumentGroupPart>();

        public DocumentGroupPart Group {
            get { return GroupField.Value; }
            set { GroupField.Value = value; }
        }
    }

    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentListPartRecord : ContentPartRecord {
        public virtual int? GroupId { get; set; }
    }
}