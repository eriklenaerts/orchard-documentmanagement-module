﻿using KVV.DocumentManagement.Models;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Orchard.Localization;

namespace KVV.DocumentManagement.Handlers {
    public class DocumentsSettingsPartHandler : ContentHandler {

        public DocumentsSettingsPartHandler(IRepository<DocumentsSettingsPartRecord> repository) {
            Filters.Add(StorageFilter.For(repository));
            Filters.Add(new ActivatingFilter<DocumentsSettingsPart>("Site"));
            Filters.Add(new TemplateFilterForRecord<DocumentsSettingsPartRecord>("DocumentsSettings", "Parts/DocumentsSettings", "Document Management"));
            T = NullLocalizer.Instance;

            OnLoaded<DocumentsSettingsPart>(InitializeSettings);
        }

        private void InitializeSettings(LoadContentContext context, DocumentsSettingsPart part) {
            if (string.IsNullOrWhiteSpace(part.DocumentsPath))
                part.DocumentsPath = "~/App_Data/Documents";
        }

        public Localizer T { get; set; }

        protected override void GetItemMetadata(GetContentItemMetadataContext context) {
            if (context.ContentItem.ContentType != "Site")
                return;
            context.Metadata.EditorGroupInfo.Add(new GroupInfo(T("Document Management")));
        }
    }
}