﻿using System.Web.Mvc;
using KVV.DocumentManagement.Services;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Environment.Extensions;

namespace KVV.DocumentManagement.Controllers {
    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentController : ControllerBase {
        private readonly IDocumentManager _documentManager;
        private readonly IOrchardServices _services;
        private readonly IDocumentAuthorizer _documentAuthorizer;
        private dynamic New { get; set; }

        public DocumentController(IDocumentManager documentManager, IOrchardServices services, IDocumentAuthorizer documentAuthorizer) {
            _documentManager = documentManager;
            _services = services;
            _documentAuthorizer = documentAuthorizer;
            New = _services.New;
        }

        public ActionResult Download(int id) {
            var document = _documentManager.Get(id, VersionOptions.Latest);

            if(!_documentAuthorizer.Authorize(document))
                return new HttpUnauthorizedResult();

            var stream = _documentManager.LoadFile(document);
            return File(stream, "application/x-binary", document.OriginalFileName);
        }
    }
}