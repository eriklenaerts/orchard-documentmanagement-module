﻿(function ($) {
    
    // We are uploading a file, so set the enctype of the form
    $("form").attr("enctype", "multipart/form-data");
    
    // Delete file event handler
    $(".document-editor").on("click", "a.delete-file", function (e) {
        e.preventDefault();
        if (!confirm($(this).data("prompt")))
            return;

        $(".document-deleted").val("true");
        $(".current-file-wrapper").slideUp();
    });
})(jQuery);