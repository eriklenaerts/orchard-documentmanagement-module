﻿using System.Linq;
using System.Web.Mvc;
using KVV.DocumentManagement.Services;
using Orchard;
using Orchard.Environment.Extensions;
using Orchard.UI.Admin;
using Orchard.UI.Navigation;
using Orchard.UI.Notify;

namespace KVV.DocumentManagement.Controllers {
    [Admin, OrchardFeature("KVV.DocumentManagement")]
    public class DocumentGroupAdminController : ControllerBase {
        private readonly IDocumentGroupManager _documentGroupManager;
        private readonly IOrchardServices _services;
        private dynamic New { get; set; }

        public DocumentGroupAdminController(IDocumentGroupManager documentGroupManager, IOrchardServices services) {
            _documentGroupManager = documentGroupManager;
            _services = services;
            New = _services.New;
        }

        public ActionResult Index(PagerParameters pagerParameters) {
            var pager = new Pager(_services.WorkContext.CurrentSite, pagerParameters);
            var groups = _documentGroupManager.Fetch(pager.GetStartIndex(), pager.PageSize).ToList();
            var count = _documentGroupManager.Count();
            var model = New.ViewModel(
                DocumentGroups: groups,
                Pager: New.Pager(pager).TotalItemCount(count)
            );
            return View(model);
        }

        public ActionResult Delete(int id) {
            var group = _documentGroupManager.Get(id);
            _documentGroupManager.Delete(group);
            _services.Notifier.Information(T("The {0} group has been removed.", group.Name));
            return RedirectToAction("Index");
        }
    }
}