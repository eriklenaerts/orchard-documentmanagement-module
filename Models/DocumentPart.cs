﻿using System;
using System.IO;
using Orchard.Autoroute.Models;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Records;
using Orchard.ContentManagement.Utilities;
using Orchard.Core.Common.Models;
using Orchard.Environment.Extensions;
using Orchard.Security;

namespace KVV.DocumentManagement.Models {
    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentPart : ContentPart<DocumentPartRecord> {
        internal LazyField<DocumentGroupPart> GroupField = new LazyField<DocumentGroupPart>();

        /// <summary>
        /// The original file name of the uploaded file
        /// </summary>
        public string OriginalFileName {
            get { return Record.OriginalFileName; }
            set { Record.OriginalFileName = value; }
        }

        /// <summary>
        /// The generated file name when the file was uploaded. This is the actual name of the file as it is stored on the server, and is unique.
        /// </summary>
        public string GeneratedFileName {
            get { return Record.GeneratedFileName; }
            set { Record.GeneratedFileName = value; }
        }

        public string Name {
            get { return ContentItem.ContentManager.GetItemMetadata(this).DisplayText; }
        }

        public DocumentGroupPart Group {
            get { return GroupField.Value; }
            set { GroupField.Value = value; }
        }

        public string FileExtension {
            get { return !string.IsNullOrWhiteSpace(OriginalFileName) ? Path.GetExtension(OriginalFileName) : null; }
        }

        public string Url {
            get { return this.As<AutoroutePart>().DisplayAlias; }
        }

        public DateTime Created {
            get { return this.As<CommonPart>().CreatedUtc.Value; }
        }

        public DateTime Modified {
            get { return this.As<CommonPart>().ModifiedUtc.Value; }
        }

        public IUser User {
            get { return this.As<CommonPart>().Owner; }
        }
    }

    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentPartRecord : ContentPartRecord {
        public virtual int GroupId { get; set; }
        public virtual string OriginalFileName { get; set; }
        public virtual string GeneratedFileName { get; set; }
    }
}