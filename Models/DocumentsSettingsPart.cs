﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Records;
using Orchard.Environment.Extensions;

namespace KVV.DocumentManagement.Models {
    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentsSettingsPart : ContentPart<DocumentsSettingsPartRecord> {
        public string DocumentsPath {
            get { return Record.DocumentsPath; }
            set { Record.DocumentsPath = value; }
        }
    }

    public class DocumentsSettingsPartRecord : ContentPartRecord {
        public virtual string DocumentsPath { get; set; }
    }
}