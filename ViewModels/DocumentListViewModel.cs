using System.Collections.Generic;
using KVV.DocumentManagement.Models;

namespace KVV.DocumentManagement.ViewModels {
    public class DocumentListViewModel {
        public IList<DocumentGroupPart> Groups { get; set; }
        public int? GroupId { get; set; }
    }
}