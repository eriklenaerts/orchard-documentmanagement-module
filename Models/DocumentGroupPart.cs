﻿using System.Collections.Generic;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Records;
using Orchard.ContentManagement.Utilities;
using Orchard.Environment.Extensions;

namespace KVV.DocumentManagement.Models {
    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentGroupPart : ContentPart<DocumentGroupPartRecord> {
        internal LazyField<IList<string>> RolesField = new LazyField<IList<string>>();

        public IList<string> Roles {
            get { return RolesField.Value; }
            set { RolesField.Value = value; }
        }

        public string Name {
            get { return ContentItem.ContentManager.GetItemMetadata(this).DisplayText; }
        }

        public static IEnumerable<string> DeserializeRoles(string rolesText) {
            return !string.IsNullOrEmpty(rolesText) ? rolesText.Split(',') : new string[0];
        }

        public static string SerializeRoles(IEnumerable<string> roles) {
            return string.Join(",", roles);
        }
    }

    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentGroupPartRecord : ContentPartRecord {
        public virtual string Roles { get; set; }
    }
}