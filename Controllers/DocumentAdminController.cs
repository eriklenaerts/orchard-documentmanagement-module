﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using KVV.DocumentManagement.Models;
using KVV.DocumentManagement.Services;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Core.Contents.Controllers;
using Orchard.Environment.Extensions;
using Orchard.UI.Admin;
using Orchard.UI.Navigation;
using Orchard.UI.Notify;

namespace KVV.DocumentManagement.Controllers {
    [Admin, OrchardFeature("KVV.DocumentManagement")]
    public class DocumentAdminController : ControllerBase {
        private readonly IDocumentManager _documentManager;
        private readonly IOrchardServices _services;
        private readonly IDocumentGroupManager _documentGroupManager;
        private dynamic New { get; set; }

        public DocumentAdminController(IDocumentManager documentManager, IOrchardServices services, IDocumentGroupManager documentGroupManager) {
            _documentManager = documentManager;
            _services = services;
            _documentGroupManager = documentGroupManager;
            New = _services.New;
        }

        [HttpGet]
        public ActionResult Index(PagerParameters pagerParameters, string q) {
            var pager = new Pager(_services.WorkContext.CurrentSite, pagerParameters);
            var documents = _documentManager.Fetch(VersionOptions.Latest, pager.GetStartIndex(), pager.PageSize, q).ToList();
            var count = _documentManager.Count(VersionOptions.Latest, q);
            var model = New.ViewModel(
                DocumentGroups: _documentGroupManager.Fetch().ToList(),
                Documents: documents.Select(x => New.Document(
                    Content: x,
                    Id: x.Id,
                    Title: x.Name,
                    Url: x.Url,
                    Created: x.Created,
                    Modified: x.Modified,
                    User: x.User,
                    Group: x.Group,
                    IDocumentGroupManager: x.Group,
                    IsPublished: x.IsPublished())).ToList(),
                SearchTerm: q,
                Pager: New.Pager(pager).TotalItemCount(count)
            );
            return View(model);
        }

        [FormValueRequired("submit.Search"), HttpPost, ActionName("Index")]
        public ActionResult Search(PagerParameters pagerParameters, string q) {
            return RedirectToAction("Index", new {page = pagerParameters.Page, pageSize = pagerParameters.PageSize, q});
        }

        [FormValueRequired("submit.AssignGroup"), HttpPost, ActionName("Index")]
        public ActionResult AssignGroup(PagerParameters pagerParameters, IEnumerable<int> documentIds, int? groupId) {

            if(groupId == null)
                _services.Notifier.Error(T("Please select a group."));

            if(documentIds == null)
                _services.Notifier.Error(T("Please check the documents that you want the group to assign to."));

            if (groupId != null && documentIds != null) {
                var documents = _services.ContentManager.GetMany<DocumentPart>(documentIds, VersionOptions.Latest, QueryHints.Empty).ToList();
                var group = _services.ContentManager.Get<DocumentGroupPart>(groupId.Value);
                _documentManager.AssignGroup(documents, group);
                _services.Notifier.Information(T("The selected documents have been assigned to the {0} group.", group.Name));
            }

            return RedirectToAction("Index", new { page = pagerParameters.Page, pageSize = pagerParameters.PageSize});
        }

        public ActionResult Delete(int id) {
            var document = _documentManager.Get(id, VersionOptions.Latest);
            _documentManager.Delete(document);
            _services.Notifier.Information(T("The {0} document has been removed.", document.Name));
            return RedirectToAction("Index");
        }

        public ActionResult Download(int id) {
            var document = _documentManager.Get(id, VersionOptions.Latest);
            var stream = _documentManager.LoadFile(document);
            return File(stream, "application/x-binary", document.OriginalFileName);
        }

        public ActionResult Publish(int id) {
            var document = _documentManager.Get(id, VersionOptions.Draft);
            _documentManager.Publish(document);
            _services.Notifier.Information(T("The {0} document has been successfully published.", document.Name));
            return RedirectToAction("Index");
        }

        public ActionResult Unpublish(int id) {
            var document = _documentManager.Get(id, VersionOptions.Published);
            _documentManager.Unpublish(document);
            _services.Notifier.Information(T("The {0} document has been successfully unpublished.", document.Name));
            return RedirectToAction("Index");
        }
    }
}