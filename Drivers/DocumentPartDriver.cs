﻿using System.Linq;
using System.Web.Routing;
using KVV.DocumentManagement.Models;
using KVV.DocumentManagement.Services;
using KVV.DocumentManagement.ViewModels;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.Environment.Extensions;

namespace KVV.DocumentManagement.Drivers {
    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentPartDriver : ContentPartDriver<DocumentPart> {
        private readonly IDocumentGroupManager _documentGroupManager;
        private readonly RequestContext _requestContext;
        private readonly IDocumentManager _documentManager;

        public DocumentPartDriver(IDocumentGroupManager documentGroupManager, RequestContext requestContext, IDocumentManager documentManager) {
            _documentGroupManager = documentGroupManager;
            _requestContext = requestContext;
            _documentManager = documentManager;
        }

        protected override string Prefix {
            get { return "Document"; }
        }

        protected override DriverResult Display(DocumentPart part, string displayType, dynamic shapeHelper) {
            return ContentShape("Parts_Document", () => shapeHelper.Parts_Document());
        }

        protected override DriverResult Editor(DocumentPart part, dynamic shapeHelper) {
            return Editor(part, null, shapeHelper);
        }

        protected override DriverResult Editor(DocumentPart part, IUpdateModel updater, dynamic shapeHelper) {
            var viewModel = new DocumentViewModel {
                Groups = _documentGroupManager.Fetch().ToList(),
                CurrentDocument = part
            };

            if (updater != null) {
                if (updater.TryUpdateModel(viewModel, Prefix, new[]{"GroupId", "Delete"}, null)) {
                    part.Group = _documentGroupManager.Get(viewModel.GroupId.Value);

                    if(viewModel.Delete)
                        DeleteCurrentFile(part);

                    MaybePersistUploadedFile(part);
                }
            }
            else {
                viewModel.GroupId = part.Group != null ? part.Group.Id : default(int?);
            }

            return ContentShape("Parts_Document_Edit", () => shapeHelper.EditorTemplate(TemplateName: "Parts/Document", Model: viewModel, Prefix: Prefix));
        }

        private void DeleteCurrentFile(DocumentPart part) {
            _documentManager.DeleteFile(part);
        }

        private void MaybePersistUploadedFile(DocumentPart part) {
            var file = _requestContext.HttpContext.Request.Files["DocumentFile"];

            if (file == null || file.ContentLength == 0)
                return;

            _documentManager.StoreFile(part, file);
        }
    }
}