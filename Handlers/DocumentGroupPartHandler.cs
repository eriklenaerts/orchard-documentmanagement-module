﻿using System.Collections.Generic;
using System.Linq;
using KVV.DocumentManagement.Models;
using KVV.DocumentManagement.Services;
using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Orchard.Environment.Extensions;
using Orchard.Roles.Models;

namespace KVV.DocumentManagement.Handlers {
    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentGroupPartHandler : ContentHandler {
        private readonly IRoleManager _roleManager;

        public DocumentGroupPartHandler(IRepository<DocumentGroupPartRecord> repository, IRoleManager roleManager) {
            _roleManager = roleManager;
            Filters.Add(StorageFilter.For(repository));
            OnActivated<DocumentGroupPart>(PropertyHandlers);
        }

        private void PropertyHandlers(ActivatedContentContext context, DocumentGroupPart part) {
            part.RolesField.Loader(x => LoadRoles(part));
            part.RolesField.Setter(x => { part.Record.Roles = DocumentGroupPart.SerializeRoles(x); return x; });
        }

        private IList<string> LoadRoles(DocumentGroupPart part) {
            var rolesText = part.Record.Roles;
            if(rolesText == null)
                return new List<string>(0);

            return DocumentGroupPart.DeserializeRoles(rolesText).ToList();
        }
    }
}