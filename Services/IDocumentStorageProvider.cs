﻿using System.IO;
using KVV.DocumentManagement.Models;
using Orchard;

namespace KVV.DocumentManagement.Services {
    public interface IDocumentStorageProvider : IDependency {
        string GetPath(DocumentPart document);
        void Store(DocumentPart document, Stream inputStream);
        Stream Load(DocumentPart document);
        void Delete(DocumentPart document);
    }
}