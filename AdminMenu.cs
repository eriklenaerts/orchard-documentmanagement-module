﻿using Orchard;
using Orchard.Environment.Extensions;
using Orchard.UI.Navigation;

namespace KVV.DocumentManagement {
    [OrchardFeature("KVV.DocumentManagement")]
    public class AdminMenu : Component, INavigationProvider {
        public string MenuName { get { return "admin"; } }

        public void GetNavigation(NavigationBuilder builder) {

            builder
                .AddImageSet("documentmanagement")
                .Add(T("Documents"), "3", documents => documents
                    .LinkToFirstChild(true)
                    .Add(T("Create Document"), "1", item => item.Action("Create", "Admin", new { area = "Contents", id = "Document" }).AddClass("create-document"))
                    .Add(T("Documents"), "2", item => item.Action("Index", "DocumentAdmin", new { area = "KVV.DocumentManagement" }))
                    .Add(T("Groups"), "3", item => item.Action("Index", "DocumentGroupAdmin", new { area = "KVV.DocumentManagement" }))
                );
        }
    }
}