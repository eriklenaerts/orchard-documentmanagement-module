﻿using System.Web.Mvc;
using Orchard.Localization;

namespace KVV.DocumentManagement.Controllers {
    public abstract class ControllerBase : Controller {
        public Localizer T { get; set; }

        protected ControllerBase() {
            T = NullLocalizer.Instance;
        }
    }
}