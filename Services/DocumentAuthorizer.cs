﻿using System.Linq;
using KVV.DocumentManagement.Models;
using Orchard;
using Orchard.Environment.Extensions;
using Orchard.Security;

namespace KVV.DocumentManagement.Services {
    public interface IDocumentAuthorizer : IDependency {
        bool Authorize(IUser user, DocumentGroupPart group);
        bool Authorize(IUser user, DocumentPart document);
        bool Authorize(DocumentGroupPart group);
        bool Authorize(DocumentPart document);
    }

    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentAuthorizer : IDocumentAuthorizer {
        private readonly IRoleManager _roleManager;
        private readonly IWorkContextAccessor _wca;
        
        public DocumentAuthorizer(IRoleManager roleManager, IWorkContextAccessor wca) {
            _roleManager = roleManager;
            _wca = wca;
        }

        protected IUser CurrentUser {
            get { return _wca.GetContext().CurrentUser; }
        }

        public bool Authorize(IUser user, DocumentGroupPart group) {
            if (user != null && user.UserName == _wca.GetContext().CurrentSite.SuperUser)
                return true;

            var userRoles =  _roleManager.GetUserRoles(user);
            var groupRoles = group.Roles;

            return userRoles.Any(groupRoles.Contains);
        }

        public bool Authorize(IUser user, DocumentPart document) {
            return Authorize(user, document.Group);
        }

        public bool Authorize(DocumentGroupPart group) {
            return Authorize(CurrentUser, group);
        }

        public bool Authorize(DocumentPart document) {
            return Authorize(CurrentUser, document);
        }
    }
}