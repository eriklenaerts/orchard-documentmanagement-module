﻿using System.Linq;
using KVV.DocumentManagement.Models;
using KVV.DocumentManagement.Services;
using KVV.DocumentManagement.ViewModels;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.Environment.Extensions;

namespace KVV.DocumentManagement.Drivers {
    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentListPartDriver : ContentPartDriver<DocumentListPart> {
        private readonly IDocumentGroupManager _documentGroupManager;
        private readonly IDocumentManager _documentManager;
        private readonly IDocumentAuthorizer _documentAuthorizer;

        public DocumentListPartDriver(IDocumentGroupManager documentGroupManager, IDocumentManager documentManager, IDocumentAuthorizer documentAuthorizer) {
            _documentGroupManager = documentGroupManager;
            _documentManager = documentManager;
            _documentAuthorizer = documentAuthorizer;
        }

        protected override string Prefix {
            get { return "DocumentList"; }
        }

        protected override DriverResult Display(DocumentListPart part, string displayType, dynamic shapeHelper) {
            if (part.Record.GroupId == null)
                return new DriverResult();
            
            return ContentShape("Parts_DocumentList", () => {

                if (!_documentAuthorizer.Authorize(part.Group))
                    return shapeHelper.Parts_DocumentList_AccessDenied();

                return shapeHelper.Parts_DocumentList(
                    DocumentGroup: part.Group,
                    Documents: _documentManager.GetByGroup(part.Group, VersionOptions.Published).ToList());
            });
        }

        protected override DriverResult Editor(DocumentListPart part, dynamic shapeHelper) {
            return Editor(part, null, shapeHelper);
        }

        protected override DriverResult Editor(DocumentListPart part, IUpdateModel updater, dynamic shapeHelper) {
            var viewModel = new DocumentListViewModel {
                Groups = _documentGroupManager.Fetch().ToList(),
                GroupId = part.Record.GroupId
            };

            if (updater != null) {
                if (updater.TryUpdateModel(viewModel, Prefix, new[]{"GroupId"}, null)) {
                    part.Group = viewModel.GroupId != null ? _documentGroupManager.Get(viewModel.GroupId.Value) : null;
                }
            }

            return ContentShape("Parts_DocumentList_Edit", () => shapeHelper.EditorTemplate(TemplateName: "Parts/DocumentList", Model: viewModel, Prefix: Prefix));
        }
    }
}