﻿using Orchard.ContentManagement.MetaData;
using Orchard.Core.Contents.Extensions;
using Orchard.Data.Migration;
using Orchard.Environment.Extensions;

namespace KVV.DocumentManagement {
    [OrchardFeature("KVV.DocumentManagement")]
    public class Migrations : DataMigrationImpl {
        
        public int Create() {

            /****************************************************************************
             * DocumentGroup
             * **************************************************************************/
            SchemaBuilder.CreateTable("DocumentGroupPartRecord", table => table
                .ContentPartRecord()
                .Column<string>("Roles", c => c.Unlimited()));

            ContentDefinitionManager.AlterPartDefinition("DocumentGroupPart", part => part.Attachable(false));
            ContentDefinitionManager.AlterTypeDefinition("DocumentGroup", type => type
                .WithPart("CommonPart")
                .WithPart("TitlePart")
                .WithPart("AutoroutePart", builder => builder
                        .WithSetting("AutorouteSettings.AllowCustomPattern", "true")
                        .WithSetting("AutorouteSettings.AutomaticAdjustmentOnEdit", "true")
                        .WithSetting("AutorouteSettings.PatternDefinitions", "[{Name:'Title', Pattern: 'document/{Content.Slug}', Description: 'document/my-document-group'}]")
                        .WithSetting("AutorouteSettings.DefaultPatternIndex", "0"))
                .WithPart("DocumentGroupPart"));

            /****************************************************************************
             * Document
             * **************************************************************************/
            SchemaBuilder.CreateTable("DocumentPartRecord", table => table
                .ContentPartRecord()
                .Column<int>("GroupId")
                .Column<string>("OriginalFileName", c => c.WithLength(256))
                .Column<string>("GeneratedFileName", c => c.WithLength(256)));

            ContentDefinitionManager.AlterPartDefinition("DocumentPart", part => part.Attachable(false));
            ContentDefinitionManager.AlterTypeDefinition("Document",
                cfg => cfg
                    .WithPart("CommonPart")
                    .WithPart("TitlePart")
                    .WithPart("AutoroutePart", builder => builder
                        .WithSetting("AutorouteSettings.AllowCustomPattern", "true")
                        .WithSetting("AutorouteSettings.AutomaticAdjustmentOnEdit", "true")
                        .WithSetting("AutorouteSettings.PatternDefinitions", "[{Name:'DocumentGroup and Title', Pattern: '{Content.Container.Path}/{Content.Slug}', Description: 'document/my-document-group/my-document'}]")
                        .WithSetting("AutorouteSettings.DefaultPatternIndex", "0"))
                    .WithPart("DocumentPart")
                    .Draftable()
                );

            /****************************************************************************
             * DocumentListPart
             * **************************************************************************/
            SchemaBuilder.CreateTable("DocumentListPartRecord", table => table
                .ContentPartRecord()
                .Column<int>("GroupId"));

            ContentDefinitionManager.AlterPartDefinition("DocumentListPart", part => part.Attachable());
            ContentDefinitionManager.AlterTypeDefinition("DocumentListWidget", widget => widget
                .WithPart("CommonPart")
                .WithPart("WidgetPart")
                .WithPart("DocumentListPart")
                .WithSetting("Stereotype", "Widget")
                .Creatable(false)
                .Draftable(false));

            /****************************************************************************
             * Settings
             * **************************************************************************/
            SchemaBuilder.CreateTable("DocumentsSettingsPartRecord", table => table
                .ContentPartRecord()
                .Column<string>("DocumentsPath", c => c.WithLength(256)));

            ContentDefinitionManager.AlterPartDefinition("DocumentsSettingsPart", part => part.Attachable(false));

            return 1;
        }
    }
}