using System;
using System.Collections.Generic;
using System.Linq;
using KVV.DocumentManagement.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Environment.Extensions;
using Orchard.Roles.Models;
using Orchard.Security;

namespace KVV.DocumentManagement.Security {
    [OrchardFeature("KVV.DocumentManagement")]
    public class AuthorizationEventHandler : IAuthorizationServiceEventHandler {
        private readonly IWorkContextAccessor _workContextAccessor;
        private static readonly string[] AnonymousRole = new[] { "Anonymous" };
        private static readonly string[] AuthenticatedRole = new[] { "Authenticated" };

        public AuthorizationEventHandler(IWorkContextAccessor workContextAccessor) {
            _workContextAccessor = workContextAccessor;
        }

        public void Checking(CheckAccessContext context) { }
        public void Adjust(CheckAccessContext context) { }

        public void Complete(CheckAccessContext context) {

            if (!String.IsNullOrEmpty(_workContextAccessor.GetContext().CurrentSite.SuperUser) 
                && context.User != null
                && String.Equals(context.User.UserName, _workContextAccessor.GetContext().CurrentSite.SuperUser, StringComparison.Ordinal)) {
                context.Granted = true;
                return;
            }

            if (context.Content == null) return;
            var part = context.Content.As<DocumentPart>();
            if (part == null)return;

            var authorizedRoles = part.Group.Roles.ToList();

            // determine what set of roles should be examined by the access check
            IEnumerable<string> rolesToExamine;
            if (context.User == null) {
                rolesToExamine = AnonymousRole;
            }
            else if (context.User.Has<IUserRoles>()) {
                // the current user is not null, so get his roles and add "Authenticated" to it
                rolesToExamine = context.User.As<IUserRoles>().Roles;

                // when it is a simulated anonymous user in the admin
                if (!rolesToExamine.Contains(AnonymousRole[0])) {
                    rolesToExamine = rolesToExamine.Concat(AuthenticatedRole);
                }
            }
            else {
                // the user is not null and has no specific role, then it's just "Authenticated"
                rolesToExamine = AuthenticatedRole;
            }
            
            context.Granted = rolesToExamine.Any(x => authorizedRoles.Contains(x, StringComparer.OrdinalIgnoreCase));
            context.Adjusted = true;
        }
    }
}