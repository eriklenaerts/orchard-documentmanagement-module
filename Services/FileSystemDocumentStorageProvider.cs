﻿using System;
using System.IO;
using System.Web;
using System.Web.Hosting;
using KVV.DocumentManagement.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Environment.Extensions;

namespace KVV.DocumentManagement.Services {
    [OrchardFeature("KVV.DocumentManagement")]
    public class FileSystemDocumentStorageProvider : IDocumentStorageProvider {
        private readonly IOrchardServices _services;

        public FileSystemDocumentStorageProvider(IOrchardServices services) {
            _services = services;
        }

        public string GetPath(DocumentPart document) {
            var settings = _services.WorkContext.CurrentSite.As<DocumentsSettingsPart>();
            EnsureGeneratedFileName(document);
            var path = VirtualPathUtility.AppendTrailingSlash(settings.DocumentsPath) + document.GeneratedFileName;
            return HostingEnvironment.MapPath(path);
        }

        public void Store(DocumentPart document, Stream inputStream) {
            SaveStream(GetPath(document), inputStream);
        }

        public Stream Load(DocumentPart document) {
            return File.OpenRead(GetPath(document));
        }

        public void Delete(DocumentPart document) {
            var path = GetPath(document);

            if(File.Exists(path))
                File.Delete(path);

            document.OriginalFileName = null;
            document.GeneratedFileName = null;
        }

        public void SaveStream(string path, Stream inputStream) {
            EnsureDirectory(path);
            using (var outputStream = File.Open(path, FileMode.Create, FileAccess.Write)) {
                var buffer = new byte[8192];
                inputStream.Position = 0;
                while (true) {
                    var length = inputStream.Read(buffer, 0, buffer.Length);
                    if (length <= 0)
                        break;
                    outputStream.Write(buffer, 0, length);
                }
            }
        }

        private void EnsureGeneratedFileName(DocumentPart document) {
            if (string.IsNullOrWhiteSpace(document.GeneratedFileName)) {
                var extension = Path.GetExtension(document.OriginalFileName);
                document.GeneratedFileName = string.Format("{0}{1}", Guid.NewGuid(), extension);
            }
        }

        private static void EnsureDirectory(string path) {
            var directory = Path.GetDirectoryName(path);

            if (!Directory.Exists(directory)) {
                Directory.CreateDirectory(directory);
            }
        }
    }
}