﻿using System.Collections.Generic;
using System.Linq;
using Orchard;
using Orchard.Data;
using Orchard.Environment.Extensions;
using Orchard.Roles.Models;
using Orchard.Security;

namespace KVV.DocumentManagement.Services {
    public interface IRoleManager : IDependency {
        IEnumerable<string> GetRoles(IEnumerable<int> ids);
        IEnumerable<string> GetRoles();
        IEnumerable<string> GetUserRoles(IUser user);
        RoleRecord GetRole(string name);
    }

    [OrchardFeature("KVV.DocumentManagement")]
    public class RoleManager : IRoleManager {
        private readonly IRepository<RoleRecord> _roleRepository;
        private readonly IRepository<UserRolesPartRecord> _userRolesRepository;
        private const string AnonymousRole = "Anonymous";
        private const string AuthenticatedRole = "Authenticated";

        public RoleManager(IRepository<RoleRecord> roleRepository, IRepository<UserRolesPartRecord> userRolesRepository) {
            _roleRepository = roleRepository;
            _userRolesRepository = userRolesRepository;
        }

        public IEnumerable<string> GetRoles(IEnumerable<int> ids) {
            return _roleRepository.Fetch(x => ids.Contains(x.Id)).Select(x => x.Name);
        }

        public IEnumerable<string> GetRoles() {
            return _roleRepository.Table.Select(x => x.Name);
        }

        public IEnumerable<string> GetUserRoles(IUser user) {
            var roles = user != null ? _userRolesRepository.Fetch(x => x.UserId == user.Id).Select(x => x.Role.Name).ToList() : new List<string>() { AnonymousRole };

            if( user != null && !roles.Contains(AuthenticatedRole))
                roles.Add(AuthenticatedRole);

            return roles;
        }

        public RoleRecord GetRole(string name) {
            return _roleRepository.Get(x => x.Name == name);
        }
    }

}