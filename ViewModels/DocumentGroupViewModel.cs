﻿using System.Collections.Generic;

namespace KVV.DocumentManagement.ViewModels {
    public class DocumentGroupViewModel {
        public IList<DocumentGroupRoleEntry> Roles { get; set; }

        public DocumentGroupViewModel() {
            Roles = new List<DocumentGroupRoleEntry>();
        }
    }

    public class DocumentGroupRoleEntry {
        public string RoleName { get; set; }
        public bool Granted { get; set; }
    }
}