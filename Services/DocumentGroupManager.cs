﻿using System.Collections.Generic;
using KVV.DocumentManagement.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Core.Title.Models;
using Orchard.Environment.Extensions;

namespace KVV.DocumentManagement.Services {
    public interface IDocumentGroupManager : IDependency {
        IEnumerable<DocumentGroupPart> Fetch();
        IEnumerable<DocumentGroupPart> Fetch(int skip, int count);
        int Count();
        DocumentGroupPart Get(int id);
        void Delete(DocumentGroupPart group);
    }

    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentGroupManager : IDocumentGroupManager {
        private readonly IContentManager _contentManager;

        public DocumentGroupManager(IContentManager contentManager) {
            _contentManager = contentManager;
        }

        public IEnumerable<DocumentGroupPart> Fetch() {
            return GetGroupsQuery().List();
        }

        public IEnumerable<DocumentGroupPart> Fetch(int skip, int count) {
            return GetGroupsQuery().Slice(skip, count);
        }

        public int Count() {
            return GetGroupsQuery().Count();
        }

        public DocumentGroupPart Get(int id) {
            return _contentManager.Get<DocumentGroupPart>(id);
        }

        public void Delete(DocumentGroupPart group) {
            _contentManager.Remove(group.ContentItem);
        }

        private IContentQuery<DocumentGroupPart> GetGroupsQuery() {
            return _contentManager.Query<DocumentGroupPart>("DocumentGroup").Join<TitlePartRecord>().OrderBy(x => x.Title);
        }
    }

}