﻿using System.Web.Routing;
using KVV.DocumentManagement.Models;
using KVV.DocumentManagement.Services;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.Core.Common.Models;
using Orchard.Data;
using Orchard.Environment.Extensions;

namespace KVV.DocumentManagement.Handlers {
    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentPartHandler : ContentHandler {
        private readonly IDocumentGroupManager _documentGroupManager;

        public DocumentPartHandler(IRepository<DocumentPartRecord> repository, IDocumentGroupManager documentGroupManager) {
            _documentGroupManager = documentGroupManager;
            Filters.Add(StorageFilter.For(repository));
            OnActivated<DocumentPart>(PropertyHandlers);
            OnGetContentItemMetadata<DocumentPart>(GetDocumentMetaData);
        }

        private void GetDocumentMetaData(GetContentItemMetadataContext context, DocumentPart part) {
            context.Metadata.DisplayRouteValues = new RouteValueDictionary {
                {"area", "KVV.DocumentManagement"},
                {"controller", "Document"},
                {"action", "Download"},
                {"id", part.Id}
            };
        }

        private void PropertyHandlers(ActivatedContentContext context, DocumentPart part) {
            part.GroupField.Loader(x => {
                var group = _documentGroupManager.Get(part.Record.GroupId);
                part.As<CommonPart>().Container = group;
                return group;
            });

            part.GroupField.Setter(x => {
                part.Record.GroupId = x != null ? x.Id : 0;
                part.As<CommonPart>().Container = x;
                return x;
            });
        }
    }
}