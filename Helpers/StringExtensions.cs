﻿namespace KVV.DocumentManagement.Helpers {
    public static class StringExtensions {
         public static string FileIconClass(this string fileExtension) {
             return "file-icon " + (!string.IsNullOrWhiteSpace(fileExtension) ? fileExtension.Replace(".", "").ToLower() : "default");
         }
    }
}