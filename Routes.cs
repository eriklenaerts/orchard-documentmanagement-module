﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Orchard.Environment.Extensions;
using Orchard.Mvc.Routes;

namespace KVV.DocumentManagement {
    [OrchardFeature("KVV.DocumentManagement")]
    public class Routes : IRouteProvider {
        public IEnumerable<RouteDescriptor> GetRoutes() {
            yield return CreateRoute("Admin/Documents", "DocumentAdmin", "Index");
            yield return CreateRoute("Admin/Documents/Groups", "DocumentGroupAdmin", "Index");
            yield return CreateRoute("Admin/Documents/Download/{id}", "DocumentAdmin", "Download");
            yield return CreateRoute("Admin/Documents/Publish/{id}", "DocumentAdmin", "Publish");
            yield return CreateRoute("Admin/Documents/Unpublish/{id}", "DocumentAdmin", "Unpublish");
            yield return CreateRoute("Documents/Download/{id}", "Document", "Download");
        }

        public void GetRoutes(ICollection<RouteDescriptor> routes) {
            foreach (var routeDescriptor in GetRoutes())
                routes.Add(routeDescriptor);
        }

        private static RouteDescriptor CreateRoute(string url, string controllerName, string actionName) {
            return new RouteDescriptor {
                Route = new Route(
                    url,
                    new RouteValueDictionary {
                                                {"area", "KVV.DocumentManagement"},
                                                {"controller", controllerName},
                                                {"action", actionName}
                                            },
                    new RouteValueDictionary(),
                    new RouteValueDictionary {
                                                {"area", "KVV.DocumentManagement"}
                                            },
                    new MvcRouteHandler())
            };
        }
    }
}