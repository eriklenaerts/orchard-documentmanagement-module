﻿Name: KVV Document Management
AntiForgery: enabled
Author: Skywalker Software Development commissioned by E2 Partners
Website: http://skywalkersoftwaredevelopment.net
Version: 1.0
OrchardVersion: 1.6
Features:
    KVV.DocumentManagement:
        Name: KVV Document Management
        Description: Introduces the Document and related content types, allowing the user to group documents and render them on the web site based on roles.
        Category: Document Management
        Dependencies: Orchard.Roles, Orchard.Autoroute, Orchard.jQuery