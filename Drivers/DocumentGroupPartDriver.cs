﻿using System.Linq;
using KVV.DocumentManagement.Models;
using KVV.DocumentManagement.Services;
using KVV.DocumentManagement.ViewModels;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.Environment.Extensions;

namespace KVV.DocumentManagement.Drivers {
    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentGroupPartDriver : ContentPartDriver<DocumentGroupPart> {
        private readonly IRoleManager _roleManager;

        public DocumentGroupPartDriver(IRoleManager roleManager) {
            _roleManager = roleManager;
        }

        protected override string Prefix {
            get { return "DocumentGroup"; }
        }

        protected override DriverResult Editor(DocumentGroupPart part, dynamic shapeHelper) {
            return Editor(part, null, shapeHelper);
        }

        protected override DriverResult Editor(DocumentGroupPart part, IUpdateModel updater, dynamic shapeHelper) {
            var viewModel = new DocumentGroupViewModel();

            if (updater != null) {
                if (updater.TryUpdateModel(viewModel, Prefix, new[]{"Roles"}, null)) {
                    var roleNames = viewModel.Roles.Where(x => x.Granted).Select(x => x.RoleName).ToList();
                    part.Roles = roleNames;
                }
            }
            else {
                viewModel.Roles = _roleManager.GetRoles().Select(x => new DocumentGroupRoleEntry {
                    RoleName = x,
                    Granted = part.Roles.Any(r => r == x)
                }).ToList();
            }

            return ContentShape("Parts_DocumentGroup_Edit", () => shapeHelper.EditorTemplate(TemplateName: "Parts/DocumentGroup", Model: viewModel, Prefix: Prefix));
        }
    }
}