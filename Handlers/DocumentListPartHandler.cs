﻿using KVV.DocumentManagement.Models;
using KVV.DocumentManagement.Services;
using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Orchard.Environment.Extensions;

namespace KVV.DocumentManagement.Handlers {
    [OrchardFeature("KVV.DocumentManagement")]
    public class DocumentListPartHandler : ContentHandler {
        private readonly IDocumentGroupManager _documentGroupManager;

        public DocumentListPartHandler(IRepository<DocumentListPartRecord> repository, IDocumentGroupManager documentGroupManager) {
            _documentGroupManager = documentGroupManager;
            Filters.Add(StorageFilter.For(repository));
            OnActivated<DocumentListPart>(PropertyHandlers);
        }

        private void PropertyHandlers(ActivatedContentContext context, DocumentListPart part) {
            part.GroupField.Loader(x => part.Record.GroupId != null ? _documentGroupManager.Get(part.Record.GroupId.Value) : null);

            part.GroupField.Setter(x => {
                part.Record.GroupId = x != null ? x.Id : default(int?);
                return x;
            });
        }
    }
}